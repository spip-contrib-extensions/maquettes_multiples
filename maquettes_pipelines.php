<?php

function maquettes_header_prive($flux) {
	$flux .= recuperer_fond("inc/head_maquettes_prive");
	return $flux;
}


function maquettes_insert_head_css($flux) {
	$flux = "\n<link rel='stylesheet' type='text/css' media='all' href='".find_in_path("css/reset.css")."'>\n"
				. recuperer_fond("inc/head_maquettes")."\n".$flux;


	return $flux;
}

