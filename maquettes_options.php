<?php

if (!defined('_AUTOBR')) define('_AUTOBR', '');
if (!defined('_NOTES_OUVRE_REF')) define('_NOTES_OUVRE_REF', "<sup class='ref_note'>");
if (!defined('_NOTES_FERME_REF')) define('_NOTES_FERME_REF', "</sup>");
if (!defined('_NOTES_OUVRE_NOTE')) define('_NOTES_OUVRE_NOTE', "<sup class='lien_note'>");
if (!defined('_NOTES_FERME_NOTE')) define('_NOTES_FERME_NOTE', "</sup>");

if (!defined('_SPIP_DOC_INTITULES_ALIGN')) define('_SPIP_DOC_INTITULES_ALIGN', "top");


if (!defined('_SPIP_MAQUETTES_CSS_MANUELS')) define('_SPIP_MAQUETTES_CSS_MANUELS', "0");
if (!$GLOBALS["dossier_squelettes"]) $GLOBALS["dossier_squelettes"] = "squelettes";