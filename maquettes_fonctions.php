<?php


if (!function_exists("mini_html")) {

	function mini_html($texte) {
		$texte = str_replace("application/mp4", "video/mp4", $texte);
	
		$texte = preg_replace(",\ +,", " ", $texte);
		$texte = preg_replace(",\n[\t|\ ]*,", "\n", $texte);
		$texte = preg_replace(",\n+,", "\n", $texte);
			
		$texte = preg_replace(",style='width:[0-9]+px;',", "", $texte);
		
		$texte = str_replace("<script type='text/javascript' src=''></script>", "", $texte);
		$texte = str_replace("<script type='text/javascript' src=", "<script async type='text/javascript' src=", $texte);
		
		
		
		$texte = diversifier_notes($texte);
		
		return trim($texte);
	}
	
}
if (!function_exists("image_100p")) {
	function _image_100p ($reg) {
		$img = $reg[1];
		$img = vider_attribut($img, "width");
		$img = vider_attribut($img, "height");
		$img = vider_attribut($img, "style");
		
		$img = inserer_attribut($img, "style", "max-width: 100%; height: auto;");
		return $img;

	}
	function image_100p ($texte) {
		return preg_replace_callback(",(<img\ [^>]*>),", "_image_100p", $texte);
		
	}

}

function diversifier_notes ($texte) {

	$texte = str_replace(" rel='footnote'", '', $texte);

	return $texte;
}


function filtrer_forcer_css($reg) {
	$lien = $reg[1];
	
	$lien = preg_replace(",\?[0-9]*$,", "", $lien);

	return "<style>".file_get_contents($lien)."</style>";
}


function forcer_css($flux) {
	return preg_replace_callback(",<link rel=\'stylesheet\' href=[\"\']([^\"\']*)[\"\'].[^>]*>,", "filtrer_forcer_css", $flux );
}

